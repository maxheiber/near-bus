import * as bus from './busApi'
import * as device from './deviceApi'

export const fetchPredictions = bus.fetchPredictions 
export const fetchStops = bus.fetchStops
export const location = device.location
