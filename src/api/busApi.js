"use strict"
import { WMATA_KEY } from '../constants'
import { nearbyStopsSchema, predictionsSchema } from './schemas'
// import jsonschema from 'jsonschema'

const headers = {
    api_key: WMATA_KEY
}

const nearbyStopsUrl = (lat, lon, radius) => 
        `https://api.wmata.com/Bus.svc/json/jStops?Lat=${lat}&Lon=${lon}&Radius=${radius}`

const predictionsUrl = stopId => 
        `https://api.wmata.com/NextBusService.svc/json/jPredictions?StopID=${stopId}` 


const fetchData = (url, schema) => {
    return fetch(url, {
        headers
    })
    .then(res => res.json())
    .then(data => {
        // const errors = validate(data, schema).errors
        // if (errors.length){
        //   return Promise.reject(errors)
        // }
        return data
    })  
}

export const fetchStops = (lat, lon, radius) => {
    const url = nearbyStopsUrl(lat, lon, radius)
    return fetchData(url, nearbyStopsSchema)
            .then(data => data.Stops || [])
}

export const fetchPredictions = stopId => {
    const url = predictionsUrl(stopId)
    return fetchData(url, predictionsSchema)
            .then(data => data.Predictions || [])
}
