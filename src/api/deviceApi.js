
// return promise for current coordinates
export const location = () => new Promise((resolve, reject) => {
    navigator.geolocation.getCurrentPosition(
        // success handler
        ({coords}) => resolve({
            lat: coords.latitude,
            lon: coords.longitude
        }),
        // failure handler
        reject
    )
})
