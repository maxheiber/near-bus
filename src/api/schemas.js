
export const nearbyStopsSchema = {
  "$schema": "http://json-schema.org/draft-04/schema#",
  "type": "object",
  "properties": {
    "Stops": {
      "type": "array",
      "items": {
        "type": "object",
        "properties": {
          "StopID": {
            "type": "string"
          },
          "Name": {
            "type": "string"
          },
          "Lon": {
            "type": "number"
          },
          "Lat": {
            "type": "number"
          },
          "Routes": {
            "type": "array",
            "items": {
              "type": "string"
            }
          }
        },
        "required": [
          "StopID",
          "Name",
          "Lon",
          "Lat",
          "Routes"
        ]
      }
    }
  },
  "required": [
    "Stops"
  ]
}

export const predictionsSchema = {
  "$schema": "http://json-schema.org/draft-04/schema#",
  "type": "object",
  "properties": {
    "StopName": {
      "type": "string"
    },
    "Predictions": {
      "type": "array",
      "items": {
        "type": "object",
        "properties": {
          "RouteID": {
            "type": "string"
          },
          "DirectionText": {
            "type": "string"
          },
          "DirectionNum": {
            "type": "string"
          },
          "Minutes": {
            "type": "integer"
          },
          "VehicleID": {
            "type": "string"
          },
          "TripID": {
            "type": "string"
          }
        },
        "required": [
          "RouteID",
          "DirectionText",
          "DirectionNum",
          "Minutes",
          "VehicleID",
          "TripID"
        ]
      }
    }
  },
  "required": [
    "StopName",
    "Predictions"
  ]
}
