import {
    FETCH_PREDICTIONS,
    FETCH_STOPS,
    FETCH_STOPS_LOADING,
    REQUEST_FETCH_STOPS,
    FETCH_STOPS_FAIL_GENERAL,
    FETCH_STOPS_FAIL_LOCATION_UNAVAILABLE
} from '../actions/actionTypes'

const DEBUG = true

const initialState = {
    stops: [],
    predictionsByStop: {},
    hasStarted: false,
    isLoading: false
}

const { assign } = Object

const reducer = (state = initialState, action = {}) => {
    let update
    switch(action.type) {
        case REQUEST_FETCH_STOPS:
            update = {hasStarted: true}
            break
        case FETCH_PREDICTIONS:
            const newPredictionsByStop = assign({}, state.predictionsByStop, action.predictionsByStop);
            update = {predictionsByStop: newPredictionsByStop}
            break
        case FETCH_STOPS:
            update = {stops: action.stops, isLoading: false}
            break
        case FETCH_STOPS_LOADING:
            update = {isLoading: true}
            break
        // todo: may want to treat these differently
        // and provide feedback to user
        case FETCH_STOPS_FAIL_GENERAL:
        case FETCH_STOPS_FAIL_LOCATION_UNAVAILABLE:
            update = {isLoading: false}
            break
        default:
            update = {}
    }

    const newState = assign({}, state, update)
    if (DEBUG) {
        // console.log("action: ", JSON.stringify(action, null, 2))
        // console.log("update: ", JSON.stringify(update, null, 2))
        // console.log("new state: ", JSON.stringify(newState, null, 2), "\n\n")
    }

    return newState
}


export default reducer