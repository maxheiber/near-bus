import { takeEvery, takeLatest } from 'redux-saga'
import { fork, call, put } from 'redux-saga/effects'
import {fetchPredictions, fetchStops} from '../api'
import { location } from '../api'
import {
  FETCH_PREDICTIONS,
  FETCH_STOPS,
  FETCH_STOPS_LOADING,
  FETCH_STOPS_FAIL_GENERAL,
  FETCH_STOPS_FAIL_LOCATION_UNAVAILABLE,
  REQUEST_FETCH_STOPS
} from "../actions/actionTypes"

function* predictionsSaga(stop) {
  const predictions = yield call(fetchPredictions, stop.StopID)
  
  yield put({
      type: FETCH_PREDICTIONS,
      predictionsByStop: {
        [stop.StopID]: predictions
      }
  })
}

function* fetchStopsSaga(action) {
  let lat, lon
  try {
    const latLon = yield call(location)
    lat = latLon.lat
    lon = latLon.lon
    console.log(lat, lon)
  }
  catch(e) {
    console.log(err)
    yield put({type: FETCH_STOPS_FAIL_LOCATION_UNAVAILABLE})
    return
  }
  try {
      yield put({type: FETCH_STOPS_LOADING})

      const stops = yield call(fetchStops, lat, lon, 500)

      yield put({type: FETCH_STOPS, stops})
      
      for(let stop of stops){
        yield fork(predictionsSaga, stop)
      }
   } catch (err) {
     console.log(err)
     yield put({type: FETCH_STOPS_FAIL_GENERAL, message: err.message})
   }
}

function* appSaga() {
  yield* takeLatest(REQUEST_FETCH_STOPS, fetchStopsSaga)
}

export default appSaga