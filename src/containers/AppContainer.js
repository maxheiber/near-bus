import React, { Component } from 'react'

import { createStore, applyMiddleware, combineReducers } from 'redux'
import { Provider } from 'react-redux'

import * as reducers from '../reducers'
import createSagaMiddleware from 'redux-saga'
import mainSaga from "../sagas/mainSaga"
import devTools from 'remote-redux-devtools'

import App from '../components/App'

const sagaMiddleware = createSagaMiddleware()

const reducer = combineReducers(reducers)
const store = createStore(
  reducer,
  applyMiddleware(sagaMiddleware)
)

sagaMiddleware.run(mainSaga)


const AppContainer = () => (
  <Provider store={store}>
    <App />
  </Provider>
)

export default AppContainer
