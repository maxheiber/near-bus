import React, { Component } from 'react';
import {
  ScrollView,
  StyleSheet,
  Text,
  View,
  RefreshControl
} from 'react-native';

import { connect } from 'react-redux'

const styles = StyleSheet.create({
  container: {
    // flex: 1,
    paddingLeft: 30,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
    paddingTop: 30,
    paddingBottom: 30
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  predictionText: {
    fontSize: 16,
    paddingLeft: 30,
    marginTop: 8,
  },
  stop: {
    marginTop: 16
  },
  stopText: {
    fontSize: 18
  }

});

const Prediction = ({ 
  DirectionText,
  Minutes,
  RouteID
}) => (
    <Text style={styles.predictionText}>{`${RouteID} ${DirectionText} ${Minutes}`}</Text>
)


const Stop = ({
  name,
  predictions
}) => {

  const renderedPredictions = predictions.map(
    (prediction, index) => <Prediction key={index} {...prediction}/> 
  )

  return (
    <View style={[styles.stop]}>
      <Text style={styles.stopText}>{`${name}`}</Text>
      <View>
        {renderedPredictions}
      </View>
    </View>
  )
}

const App = ({
  stops,
  noStopsFound,
  onRequestFetch,
  hasStarted,
  isLoading
}) => {

  // load data on opening the app
  if (!hasStarted){
    setTimeout(onRequestFetch)
  }

  const renderedStops = stops.map(stop => 
        <Stop
          key={stop.StopID}
          name={stop.Name}
          predictions={stop.predictions}
        />
  )

  return (
    <ScrollView
      contentContainerStyle={styles.container}
      refreshControl={
          <RefreshControl
            refreshing={isLoading}
            onRefresh={onRequestFetch}
          />
      }
    >
      <Text style={styles.welcome}>
        NearBus
      </Text>
      <Text>
        {isLoading ? 'Loading ...' : ''}
        {noStopsFound ? 'No stops found, please select a stop' : ''}
      </Text>
      <View>
        {renderedStops}
      </View>
    </ScrollView>
  )
}

const mapStateToProps = state => {

    const {
      hasStarted,
      isLoading,
      predictionsByStop
    } = state.bus

    // add predictions to stops
    // and skip stops with no predictions
    let stops = state.bus.stops.reduce((stops, stop) => {
        const predictions = predictionsByStop[stop.StopID]
        if (!predictions || !predictions.length){
            // ignore this stop
            return stops
        }

        const stopWithPredictions = Object.assign({}, stop, {predictions})
        return stops.concat(stopWithPredictions)
    }, [])


    return {
        stops,
        noStopsFound: hasStarted && !stops.length,
        hasStarted,
        isLoading
    }

}


const mapDispatchToProps = dispatch => ({
    onRequestFetch(){
      dispatch({
        type: 'REQUEST_FETCH_STOPS'
      })
    }
})

export default connect(mapStateToProps, mapDispatchToProps)(App)